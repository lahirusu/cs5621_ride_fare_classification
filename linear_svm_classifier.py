from preprocessing import *
from sklearn.svm import LinearSVC


def linear_svm_classifier(x_train, x_test, tripid, y_train, sample_data):

    LinearSVM = LinearSVC(penalty='l2', C=1.0, multi_class='ovr', fit_intercept=True, max_iter=100000)
    LinearSVM.fit(x_train, y_train)

    y_pred = LinearSVM.predict(x_test)

    sample_data['tripid'] = tripid
    sample_data['prediction'] = y_pred

    write_result_to_file(sample_data)


def create_model():
    train_data_set = read_data_set('Data_Set//train.csv', True)
    test_data_set = read_data_set('Data_Set//test.csv', False)
    sample_data_set = read_file('Data_Set//results.csv')

    x_train, x_test, tripid, y_train = data_pre_process(train_data_set, test_data_set)

    linear_svm_classifier(x_train, x_test, tripid, y_train, sample_data_set)


create_model()