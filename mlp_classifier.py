from preprocessing import *
from sklearn.neural_network import MLPClassifier


def mlp_classifier(x_train, x_test, tripid, y_train, sample_data):
    MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(15,), random_state=1)
    MLPClassifier.fit(x_train, y_train)

    y_pred = MLPClassifier.predict(x_test)

    sample_data['tripid'] = tripid
    sample_data['prediction'] = y_pred

    write_result_to_file(sample_data)


def create_model():
    train_data_set = read_data_set('Data_Set//train.csv', True)
    test_data_set = read_data_set('Data_Set//test.csv', False)
    sample_data_set = read_file('Data_Set//results.csv')

    x_train, x_test, tripid, y_train = data_pre_process(train_data_set, test_data_set)

    mlp_classifier(x_train, x_test, tripid, y_train, sample_data_set)


create_model()
