import numpy as np
from sklearn.ensemble import RandomForestClassifier
from preprocessing import *


def random_forest_classifier(x_train, x_test, tripid, y_train, sample_data):
    model = RandomForestClassifier()
    model.fit(x_train, y_train)

    y_pred = model.predict(x_test)

    sample_data['tripid'] = tripid
    sample_data['prediction'] = y_pred

    write_result_to_file(sample_data)


def create_model():
    train_data_set = read_data_set('Data_Set//train.csv', True)
    test_data_set = read_data_set('Data_Set//test.csv', False)
    sample_data_set = read_file('Data_Set//results.csv')

    x_train, x_test, tripid, y_train = data_pre_process(train_data_set, test_data_set)

    random_forest_classifier(x_train, x_test, tripid, y_train, sample_data_set)


create_model()
