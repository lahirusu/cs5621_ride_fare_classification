import pandas as pd
import numpy as np
import time
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.ensemble import ExtraTreesClassifier

def read_data_set(file_name, isTrainData):
	pattern = '%m/%d/%Y %H:%M'

    if isTrainData:
        df = pd.read_csv(file_name, usecols=['duration','additional_fare','meter_waiting','meter_waiting_fare','meter_waiting_till_pickup','fare','label','pickup_time','drop_time','pick_lat','pick_lon','drop_lat','drop_lon'])
        df.label = [1 if each == 'correct' else 0 for each in df.label]
    else:
        df = pd.read_csv(file_name, usecols=['tripid','duration','additional_fare','meter_waiting','meter_waiting_fare','meter_waiting_till_pickup','fare','pickup_time','drop_time','pick_lat','pick_lon','drop_lat','drop_lon'])
    
    if 'pickup_time' in df:
        df.pickup_time = [int(time.mktime(time.strptime(tt1, pattern))) for tt1 in df.pickup_time]
    if 'drop_time' in df:
        df.drop_time = [int(time.mktime(time.strptime(tt2, pattern))) for tt2 in df.drop_time]

    return df


def read_file(filename):
    return pd.read_csv(filename)

def write_result_to_file(data_set):
    data_set.to_csv('Data_Set//Results.csv', index=False)


def data_pre_process(train_data, test_data):
    # Trip ID in test set saved
    tripid = test_data['tripid']

    # Label of the testing set
    y_train = train_data['label']
    x_train = train_data
    x_train = x_train.drop(['label'], axis=1)
    x_train = x_train.replace(to_replace=np.nan, value=0)

    x_test = test_data
    x_test = x_test.drop(['tripid'], axis=1)
    x_test = x_test.replace(to_replace=np.nan, value=0)

    return x_train, x_test, tripid, y_train

	
def correlaction_check(dataFrame):
    # get correlations of each features in dataset
    corrmatrix = dataFrame.corr()
    top_corr_features = corrmatrix.index
    plt.figure(figsize=(20, 20))
    # plot heat map
    g = sns.heatmap(dataFrame[top_corr_features].corr(), annot=True, cmap="RdYlGn")


def select_kbest(X, y):
    bestfeatures = SelectKBest(score_func=chi2, k=10)
    fit = bestfeatures.fit(X, y)
    dfscores = pd.DataFrame(fit.scores_)
    dfcolumns = pd.DataFrame(X.columns)
    # concat two dataframes for better visualization
    featureScores = pd.concat([dfcolumns, dfscores], axis=1)
    featureScores.columns = ['Specs', 'Score']  # naming the dataframe columns
    print(featureScores.nlargest(12, 'Score'))  # print 10 best features


def feature_importance(X, y):
    model = ExtraTreesClassifier()
    model.fit(X, y)
    print(model.feature_importances_)  # use inbuilt class feature_importances of tree based classifiers
    # plot graph of feature importances for better visualization
    feat_importances = pd.Series(model.feature_importances_, index=X.columns)
    feat_importances.nlargest(5).plot(kind='barh')
    plt.show()

	
def create_model():
    train_data_set = read_data_set('Data_Set//train.csv', True)
    test_data_set = read_data_set('Data_Set//test.csv', False)

    x_train, x_test, tripid, y_train = data_pre_process(train_data_set, test_data_set)

    select_kbest(x_train, y_train)
    feature_importance(x_train, y_train)
    correlaction_check(x_train)


create_model()
